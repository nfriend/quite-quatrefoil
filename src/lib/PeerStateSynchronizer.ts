import { Peer } from 'peerjs';
import type { DataConnection } from 'peerjs';

export class PeerStateSynchronizer<T> {
  public hostId: string | null = null;
  public connections: DataConnection[] = [];
  public state: T;

  public get isHost(): boolean {
    return Boolean(this.hostId) && this.hostId === this.myId;
  }

  public get myId(): string | null {
    return this.peer?.id ?? null;
  }

  private peer: Peer | null = null;
  private onStateUpdatedHandlers: ((updatedState: T) => void)[] = [];

  constructor(initialState: T) {
    this.state = initialState;
  }

  private async setupConnection(connection: DataConnection): Promise<void> {
    return new Promise((resolve, reject) => {
      connection.on('open', () => {
        this.connections.push(connection);

        if (this.isHost) {
          // do a one-time update for the new connection with the current state
          connection.send(this.state);
        }

        resolve();

        connection.on('data', (newState) => {
          this.state = newState as T;
          this.onStateUpdatedHandlers.forEach((fn) => fn(this.state));

          if (this.isHost) {
            this.connections.forEach((c) => {
              if (c.connectionId !== connection.connectionId) {
                c.send(this.state);
              }
            });
          }
        });
      });

      connection.on('error', (e): void => {
        reject(`Connection error: ${e}`);
      });
    });
  }

  private async setupPeer(): Promise<Peer> {
    return new Promise((resolve, reject) => {
      const peer = new Peer();
      peer.on('open', () => {
        resolve(peer);
      });
      peer.on('connection', (connection) => {
        this.setupConnection(connection);
      });
      peer.on('error', (e) => {
        reject(`Peer error: ${e}`);
      });
    });
  }

  async host(): Promise<string> {
    this.peer = await this.setupPeer();
    this.hostId = this.peer.id;

    return this.peer.id;
  }

  async connectToHost(hostId: string): Promise<string> {
    this.peer = await this.setupPeer();
    this.hostId = hostId;

    const connection = this.peer.connect(this.hostId);
    await this.setupConnection(connection);

    return this.peer.id;
  }

  async updateState(newState: T) {
    this.state = newState;
    this.onStateUpdatedHandlers.forEach((fn) => fn(this.state));
    this.connections.forEach((connection) => connection.send(newState));
  }

  onStateUpdated(handler: (updatedState: T) => void) {
    this.onStateUpdatedHandlers.push(handler);

    // unsubscription function
    return () => {
      this.onStateUpdatedHandlers.filter((fn) => fn != handler);
    };
  }
}
