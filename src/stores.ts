import type { PeerStateSynchronizer } from '$lib/PeerStateSynchronizer';
import { writable } from 'svelte/store';
import type { GameState } from './GameState';

export const connectionInfo = writable<{
  stateSyncer: PeerStateSynchronizer<GameState> | null;
}>({
  stateSyncer: null,
});

export const initialGameState: GameState = { hostId: null, players: [], phase: 'setup' };

export const game = writable<{ state: GameState }>({ state: initialGameState });

let previousUnsubscriptionFn: (() => void) | undefined;
connectionInfo.subscribe((newConnectionInfo) => {
  previousUnsubscriptionFn?.();
  previousUnsubscriptionFn = newConnectionInfo.stateSyncer?.onStateUpdated((newState) => {
    game.set({ state: newState });
  });
});
