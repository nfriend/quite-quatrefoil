export interface Player {
  id: string;
  name: string;
  avatarSeed: number;
}

export interface GameState {
  hostId: string | null;
  players: Player[];
  phase: 'setup' | 'guessing';
}
