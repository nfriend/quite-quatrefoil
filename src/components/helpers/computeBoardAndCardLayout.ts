import type { BoardInputPosition, CardPosition, CardTablePosition } from '../Board.svelte';
import type * as CSS from 'csstype';

type CardLayouts = { [key in CardPosition]: string };
type BoardInputLayouts = { [key in BoardInputPosition]: string };

type BoardLayout = string;

type BoardAndCardLayout = {
  cardLayouts: CardLayouts;
  boardLayout: BoardLayout;
  boardInputLayous: BoardInputLayouts;
};

type CardPlacement = 'sides' | 'bottomSingleRow' | 'bottomDoubleRow';

export const objToCss = (css: CSS.Properties): string => {
  return Object.entries(css)
    .map(([key, value]) => `${key}: ${value}`)
    .join('; ');
};

export const objPropertiesToCss = <T extends string>(obj: {
  [key in T]: CSS.Properties;
}): { [key in T]: string } => {
  return (Object.entries(obj) as [T, CSS.Properties][]).reduce(
    (acc, [key, css]: [T, CSS.Properties]) => {
      acc[key] = objToCss(css);

      return acc;
    },
    {} as { [key in T]: string },
  );
};

export const computeBoardAndCardLayout = (): BoardAndCardLayout => {
  const screenWidthPx = window.innerWidth;
  const screenHeightPx = window.innerHeight;
  const screenAspectRatio = screenWidthPx / screenHeightPx;
  const screenCenterPointX = screenWidthPx / 2;
  const screenCenterPointY = screenHeightPx / 2;

  let cardPlacement: CardPlacement;
  if (screenAspectRatio < 1) {
    // for the narrowest screens
    cardPlacement = 'bottomDoubleRow';
  } else if (screenAspectRatio < 3 / 2) {
    // for square-ish screens
    cardPlacement = 'bottomSingleRow';
  } else {
    // for wide screens
    cardPlacement = 'sides';
  }

  let boardWidthPx: number;
  let boardTopPx: number;
  let boardLeftPx: number;
  let cardWidthPx: number;
  let gapWidthPx: number;

  let tableSizes: { [position in CardTablePosition]: CSS.Properties };
  if (cardPlacement === 'bottomDoubleRow') {
    boardWidthPx = Math.min(screenWidthPx, screenHeightPx * (1 / 2));
    boardTopPx = 60;
    boardLeftPx = screenCenterPointX - boardWidthPx / 2;
    cardWidthPx = boardWidthPx / 3.625;
    gapWidthPx = boardWidthPx / 50;

    const width = `${cardWidthPx / 16}rem`;

    const row1Top = `${(boardTopPx + boardWidthPx + gapWidthPx) / 16}rem`;
    const row2Top = `${(boardTopPx + boardWidthPx + gapWidthPx + cardWidthPx + gapWidthPx) / 16}rem`;

    tableSizes = {
      table1: {
        width,
        top: row1Top,
        left: `${(screenCenterPointX - gapWidthPx / 2 - cardWidthPx) / 16}rem`,
      },
      table2: {
        width,
        top: row1Top,
        left: `${(screenCenterPointX + gapWidthPx / 2) / 16}rem`,
      },
      table3: {
        width,
        top: row2Top,
        left: `${(screenCenterPointX - gapWidthPx - cardWidthPx * (3 / 2)) / 16}rem`,
      },
      table4: {
        width,
        top: row2Top,
        left: `${(screenCenterPointX - cardWidthPx / 2) / 16}rem`,
      },
      table5: {
        width,
        top: row2Top,
        left: `${(screenCenterPointX + gapWidthPx + cardWidthPx / 2) / 16}rem`,
      },
    };
  } else if (cardPlacement === 'bottomSingleRow') {
    boardWidthPx = screenHeightPx * (2 / 3);
    boardTopPx = 60;
    boardLeftPx = screenCenterPointX - boardWidthPx / 2;
    cardWidthPx = boardWidthPx / 3.625;
    gapWidthPx = boardWidthPx / 50;

    const width = `${cardWidthPx / 16}rem`;

    const rowTop = `${(boardTopPx + boardWidthPx) / 16}rem`;

    tableSizes = {
      table1: {
        width,
        top: rowTop,
        left: `${(screenCenterPointX - cardWidthPx * (5 / 2) - gapWidthPx * 2) / 16}rem`,
      },
      table2: {
        width,
        top: rowTop,
        left: `${(screenCenterPointX - cardWidthPx * (3 / 2) - gapWidthPx) / 16}rem`,
      },
      table3: {
        width,
        top: rowTop,
        left: `${(screenCenterPointX - cardWidthPx / 2) / 16}rem`,
      },
      table4: {
        width,
        top: rowTop,
        left: `${(screenCenterPointX + cardWidthPx / 2 + gapWidthPx) / 16}rem`,
      },
      table5: {
        width,
        top: rowTop,
        left: `${(screenCenterPointX + cardWidthPx * (3 / 2) + gapWidthPx * 2) / 16}rem`,
      },
    };
  } else if (cardPlacement === 'sides') {
    boardWidthPx = screenHeightPx * (8 / 10);
    boardTopPx = screenHeightPx / 2 - boardWidthPx / 2;
    boardLeftPx = screenWidthPx / 2 - boardWidthPx / 2;
    cardWidthPx = boardWidthPx / 3.625;
    gapWidthPx = boardWidthPx / 50;

    const xOffset = `${(screenCenterPointX - boardWidthPx / 2 - gapWidthPx - cardWidthPx) / 16}rem`;
    const width = `${cardWidthPx / 16}rem`;

    tableSizes = {
      table1: {
        width,
        left: xOffset,
        top: `${(screenCenterPointY - cardWidthPx - gapWidthPx) / 16}rem`,
      },
      table2: {
        width,
        left: xOffset,
        top: `${(screenCenterPointY + gapWidthPx) / 16}rem`,
      },
      table3: {
        width,
        right: xOffset,
        top: `${(screenCenterPointY - cardWidthPx * 1.5 - gapWidthPx * 2) / 16}rem`,
      },
      table4: {
        width,
        right: xOffset,
        top: `${(screenCenterPointY - cardWidthPx / 2) / 16}rem`,
      },
      table5: {
        width,
        right: xOffset,
        top: `${(screenCenterPointY + cardWidthPx / 2 + gapWidthPx * 2) / 16}rem`,
      },
    };
  } else {
    throw new Error(`Unimplemented card placement: "${cardPlacement}"`);
  }

  const boardCenterPointX = boardLeftPx + boardWidthPx / 2;
  const boardCenterPointY = boardTopPx + boardWidthPx / 2;

  const xOffset = `${(boardCenterPointX - cardWidthPx - gapWidthPx / 2) / 16}rem`;
  const width = `${cardWidthPx / 16}rem`;

  const cardLayouts: { [position in CardPosition]: CSS.Properties } = {
    boardTopLeft: {
      width,
      left: xOffset,
      top: `${(boardCenterPointY - cardWidthPx - gapWidthPx / 2) / 16}rem`,
    },
    boardTopRight: {
      width,
      right: xOffset,
      top: `${(boardCenterPointY - cardWidthPx - gapWidthPx / 2) / 16}rem`,
    },
    boardBottomLeft: {
      width,
      left: xOffset,
      top: `${(boardCenterPointY + gapWidthPx / 2) / 16}rem`,
    },
    boardBottomRight: {
      width,
      right: xOffset,
      top: `${(boardCenterPointY + gapWidthPx / 2) / 16}rem`,
    },
    ...tableSizes,
  };

  const boardLayout: CSS.Properties = {
    width: `${boardWidthPx / 16}rem`,
    top: `${boardTopPx / 16}rem`,
    left: `${boardLeftPx / 16}rem`,
  };

  const boardInputWidthPx = boardWidthPx / 3;
  const boardInputLayouts = {
    top: {
      left: `${(boardCenterPointX - boardInputWidthPx / 2) / 16}rem`,
      width: `${boardInputWidthPx / 16}rem`,
      top: `${(boardCenterPointY - boardWidthPx * (8 / 21)) / 16}rem`,
    },

    // TODO
    right: {
      width: `${boardInputWidthPx / 16}rem`,
    },
    bottom: {
      width: `${boardInputWidthPx / 16}rem`,
    },
    left: {
      width: `${boardInputWidthPx / 16}rem`,
    },
  };

  return {
    cardLayouts: objPropertiesToCss(cardLayouts),
    boardLayout: objToCss(boardLayout),
    boardInputLayous: objPropertiesToCss(boardInputLayouts),
  };
};
