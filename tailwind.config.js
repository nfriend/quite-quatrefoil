/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      mdlg: '980px',
      lg: '1080px',
      xl: '1280px',
      '2xl': '1536px',
    },
    extend: {
      fontFamily: {
        handwriting: ['Gloria Hallelujah', 'ui-sans-serif', 'system-ui'],
        card: ['Hamberger Bold', 'Gloria Hallelujah', 'ui-sans-serif', 'system-ui'],
      },
      spacing: {
        128: '32rem',
      },
      screens: {
        thinner: { raw: '(max-aspect-ratio: 4/5)' },
        'no-hover': { raw: '(hover: none)' },
      },
    },
  },
  plugins: [],
};
