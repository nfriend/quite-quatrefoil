# Super Shamrock

An online, cooperative word-guessing game, inspired by [So Clover](https://boardgamegeek.com/boardgame/329839/so-clover).

[Learn to play here!](https://youtu.be/SbnYbqztiJY?si=aJQYRE-0oJERrlj0)

<img src="static/logo.png" alt="The Super Shamrock logo" />

## Developing

```sh
yarn run dev --open
```

## Building

To create a production version of Super Shamrock:

```sh
yarn run build
```

You can preview the production build with `npm run preview`.

## Deploying

Push changes to `main`. Deployment happens magically :sparkles: with GitLab Pages.
